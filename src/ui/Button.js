import styled from 'styled-components'

const buttonColor = ({ theme, secondary, disabled }) => {
  const { white, teal, lightTeal, grey, lightGrey } = theme.colors
  let color = white
  let bgColor = teal
  let hoverBgColor = lightTeal
  if (disabled){
    color = grey
    bgColor = lightGrey
    hoverBgColor = lightGrey
  }
  else if (secondary){
    color = teal
    bgColor = white
  }
  return `
    color : ${ color };
    background-color : ${ bgColor };
    &:hover{
      background-color : ${ hoverBgColor };
    }
  `
}

const Button = styled.button.attrs({ type : 'submit' })`
  ${ buttonColor }
  font-weight: bold;
  padding: 1em 1.5em;
  font-size: 16px;
  cursor: ${ ({ disabled }) => disabled ? 'default' : 'pointer' };
  border-radius: 3px;
  outline: none;
  box-shadow: 1px 1px 1px ${ ({ theme }) => theme.colors.lightGrey };
`

export default Button
