import React from 'react'
import styled from 'styled-components'

const StyledInput = styled.input`
  padding: 1em 0;
  text-indent: 1em;
  border-radius: 5px;
  outline: none;
  border: 1px solid ${ ({ theme }) => theme.colors.teal };
  font-size: 14px;
`

const Input = ({ value, label, onChange, password }) => {
  const props = {
    value,
    placeholder : label,
    autoComplete : 'off',
    onChange : e => onChange(e.target.value),
    type : password ? 'password' : 'text'
  }
  return <StyledInput { ...props } />
}

export default Input
