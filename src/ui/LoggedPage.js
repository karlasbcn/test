import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import Button from './Button'
import Avatar from './Avatar'

const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 72px;
  min-height: 100px;
  background-color: ${ ({ theme, isAdminPage }) => theme.colors[ isAdminPage ? 'blue' : 'teal' ] };
  color: ${ ({ theme }) => theme.colors.white };
  @media screen and (max-width: 500px) {
    flex-direction: column;
    & button{
      margin: 1em 0;
    }
  }
`

const UserDataWrap = styled.div`
  height: 100%;
  color: ${ ({ theme }) => theme.colors.white };
  display: flex;
  padding-right: 4em;
  align-items: center;
  & > div:last-child{
    flex-direction: column;
    & > div{
      margin-bottom: 10px;
      &:first-child{
        margin-top: 6px;
        font-size: 18px;
      }
      &:last-child{
        ${ ({ theme, hasAdminRole }) => hasAdminRole ? `color: ${ theme.colors.yellow };` : '' }
      }
    }
  }
  }
  @media screen and (max-width: 610px) {
    padding-right: 0;
    text-align: center;
  }
`

const UserData = ({ username, phone, hasAdminRole, email, avatarUrl, uploadAvatar }) => {
  return (
    <UserDataWrap hasAdminRole={ hasAdminRole }>
      <div>
        <Avatar onSelect={ uploadAvatar } letter={ username[0] } url={ avatarUrl } />
      </div>
      <div>
        <div>{ `Welcome back, ${ username }!` }</div>
        <div>{ email }</div>
        <div>{ `Phone number: ${ phone }` }</div>
        { hasAdminRole ? <div>ADMIN</div> : null }
      </div>
    </UserDataWrap>
  )
}

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top: 2em;
  width: 100%;
  & > div{
    font-size: 36px;
    text-indent: 72px;
    color: ${ ({ theme }) => theme.colors.grey };
    &:last-child{
      margin-right: 72px;
    }
  }
  @media screen and (max-width: 610px) {
    flex-direction: column-reverse;
  }
`

const AdminLinkWrap = styled.div`
  & a, a:visited, a:link, a:hover, a:active{
    text-align: center;
    text-decoration: none;
    cursor: pointer;
    font-size: 14px;
    color: ${ ({ theme }) => theme.colors.grey };
    &:hover{
      text-decoration: underline;
    }
  }
`

const AdminLink = ({ isAdminPage }) => (
  <AdminLinkWrap>
    <Link to={ isAdminPage ? '/home' : '/home/admin'}>
      { isAdminPage ? 'Go to home page' : 'Go to admin page' }
    </Link>
  </AdminLinkWrap>
)

const LoggedPage = ({ user, signOut, isAdminPage, children, uploadAvatar }) => (
  <>
    <Header isAdminPage={ isAdminPage }>
      <UserData { ...user } uploadAvatar={ uploadAvatar }  />
      <Button secondary onClick={ signOut }>Sign out</Button>
    </Header>
    <Container>
      <div>{ children }</div>
      { user.hasAdminRole ? <AdminLink isAdminPage={ isAdminPage } /> : null }
    </Container>
  </>
)

export default LoggedPage
