import React from 'react'
import styled from 'styled-components'

const FormWrap = styled.form`
  border: 1px solid ${ ({ theme }) => theme.colors.grey };
  border-radius: 5px;
  width: 600px;
  @media screen and (max-width: 700px) {
    width: 400px;
  }
  @media screen and (max-width: 500px) {
    width: 300px;
  }
  @media screen and (max-width: 400px) {
    width: 270px;
  }
  display: flex;
  margin: 0 auto;
  margin-top: 100px;
  flex-direction: column;
  align-items: center;
  padding: 3em;
  & > *{
    width: 100%;
    margin: 1em 0;
  }
  & > div:first-child{
    text-align: center;
    font-size: 16px;
  }
  & a, a:visited, a:link, a:hover, a:active{
    text-align: center;
    text-decoration: none;
    cursor: pointer;
    color: ${ ({ theme }) => theme.colors.grey };
    &:hover{
      text-decoration: underline;
    }
  }
`

class Form extends React.Component{
  constructor(opts){
    super(opts)
    this.ref = React.createRef()
  }
  componentDidMount(){
    this.ref.current.querySelector('input').focus()
  }
  submit(e){
    e.preventDefault()
    this.props.onSubmit()
  }
  render(){
    return <FormWrap onSubmit={ e => this.submit(e) } ref={ this.ref }>
      { this.props.children }
    </FormWrap>
  }
}

export default Form
