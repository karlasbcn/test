import React, { useState, useEffect } from 'react'
import styled from 'styled-components'

const Message = styled.div`
  color: ${ ({ theme }) => theme.colors.red };
  opacity : ${ ({ show }) => show ? 1 : 0 };
  transition: all 300ms linear;
  text-align: center;
  height : ${ ({ show }) => show ? '14px;' : 0 };
  ${ ({ show }) => show ? '' : 'margin:0 !important;' }
`

const Error = ({ children }) => {
  const [ show, setShow ] = useState(false)
  useEffect(() => {
    setTimeout(() => setShow(true), 50)
    setTimeout(() => setShow(false), 4000)
  }, [])
  return <Message show={ show }>{ children }</Message>
}

export default Error
