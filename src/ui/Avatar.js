import React from 'react'
import styled from 'styled-components'

const Wrap = styled.div`
  cursor: pointer;
  height: 80px;
  width: 80px;
  border-radius: 40px;
  background-color : ${ ({ theme }) => theme.colors.grey };
  background-image : ${ ({ url }) => url ? `url(${ url })` : 'none' };
  background-size: cover;
  background-position: center;
  margin-right: 20px;
  &::after{
    content : ${ ({ url, letter }) => url ? '""' : `"${ letter }"` };
    color: ${ ({ theme }) => theme.colors.white };
    text-transform: uppercase;
    margin-left: 30px;
    @media screen and (max-width: 610px) {
      margin-left: 1px;
    }
    height: 100%;
    font-size: 34px;
    line-height: 74px;
  }
  & > input{
    visibility: hidden;
    position: absolute;
  }
`

class Avatar extends React.Component{
  constructor(opts){
    super(opts)
    this.ref = React.createRef()
  }
  openDialog(){
    this.ref.current.click()
  }
  selectImage(event){
    this.props.onSelect(event.target.files[0])
  }
  render(){
    const { url, letter } = this.props
    return (
      <Wrap url={ url } letter={ letter } onClick={ () => this.openDialog() }>
        <input onChange={ e => this.selectImage(e) } type="file" ref={ this.ref } accept="image/png, image/jpeg" />
      </Wrap>
    )
  }
}

export default Avatar
