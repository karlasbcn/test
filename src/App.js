import React from 'react'
import { Route, Router, Switch, Redirect } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { ThemeProvider } from 'styled-components'
import SignIn from './pages/SignIn'
import Home from './pages/Home'
import Admin from './pages/Admin'
import Register from './pages/Register'
import { getCurrentUser, signIn, signUp, confirmCode, signOut, uploadAvatar } from './utils/auth'
import { GlobalStyle, theme } from './utils/style'

const browserHistory = createBrowserHistory()

class App extends React.Component{
  state = { 
    user : null,
    verifyCode : false
  }
  async componentDidMount(){
    const user = await getCurrentUser()
    this.setState({ user })
  }
  async signIn(username, password){
    const ok = await signIn(username, password)
    if (ok.error){
      this.setState({ error : ok.error })
      return
    }
    const user = await getCurrentUser()
    this.setState({ 
      user,
      error : '',
      verifyCode : false 
    })
  }
  async signUp(username, password, phone, email){
    const ok = await signUp(username, password, phone, email)
    if (ok.error){
      this.setState({ error : ok.error })
      return
    }
    ok && this.setState({ 
      error : '',
      verifyCode : { username, password } 
    })
  }
  async confirmCode(code){
    const { username, password } = this.state.verifyCode
    const ok = await confirmCode(username, code)
    if (ok.error){
      this.setState({ error : ok.error })
      return
    }
    ok && this.signIn(username, password)
  }
  async signOut(){
    const ok = await signOut()
    ok && this.setState({ user : false })
  }
  async uploadAvatar(file){
    const ok = await uploadAvatar(file, this.state.user.username)
    if (ok){
      const user = await getCurrentUser()
      this.setState({ user })
    }
  }
  getPageComponent(Component, isPublic = true, isAdmin = false){
    const { user, verifyCode, error } = this.state
    let props
    if (user === null){
      return () => null
    }
    if (isPublic){
      if (!!user){
        return () => <Redirect to="/home" />
      }
      props = {
        verifyCode,
        error,
        signIn : (username, password) => this.signIn(username, password),
        signUp : (username, password, phone, email) => this.signUp(username, password, phone, email),
        confirmCode : code => this.confirmCode(code)
      }
    }
    else{
      if(!user){
        return () => <Redirect to="/" />
      }
      if (isAdmin && !user.hasAdminRole){
        return () => <Redirect to="/home" />
      }
      props = { 
        user,
        signOut : () => this.signOut(),
        uploadAvatar : file => this.uploadAvatar(file)
      }
    }
    return defaultProps => <Component { ...defaultProps } { ...props } />
  }
  render(){
    return(
      <ThemeProvider theme={ theme }>
        <>
          <Router history={ browserHistory }>
            <Switch>
              <Route exact path="/home/admin" render={ this.getPageComponent(Admin, false, true) } />
              <Route exact path="/home" render={ this.getPageComponent(Home, false) } />
              <Route exact path="/register" render={ this.getPageComponent(Register) } />
              <Route exact path="/" render={ this.getPageComponent(SignIn) } />
            </Switch>
          </Router>
          <GlobalStyle />
        </>
      </ThemeProvider>
    )
  }
}

export default App
