import React from 'react'
import LoggedPage from '../ui/LoggedPage'

const Home = ({ user, signOut, uploadAvatar }) => (
  <LoggedPage user={ user } signOut={ signOut } uploadAvatar={ uploadAvatar }>
    Home page
  </LoggedPage>
)

export default Home