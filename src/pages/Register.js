import React from 'react'
import { Link } from 'react-router-dom'
import Form from '../ui/Form'
import Button from '../ui/Button'
import Input from '../ui/Input'
import Error from '../ui/Error'

class Register extends React.Component{
  state = {
    username : '',
    password : '',
    phone : '',
    email : '',
    code : ''
  }
  submitForm(){
    const { username, password, phone, email } = this.state
    this.props.signUp(username, password, phone, email)
  }
  submitCode(){
    this.props.confirmCode(this.state.code)
  }
  renderForm(){
    const { username, password, phone, email } = this.state
    const { error } = this.props
    const disableButton = !(username.length && password.length && phone.length && email.length)
    return (
      <Form onSubmit={ () => this.submitForm() }>
        <div>Insert your username, phone number and password</div>
        <Input label="Username" value={ username } onChange={ username => this.setState({ username }) } />
        <Input label="Email address" value={ email } onChange={ email => this.setState({ email }) } />
        <Input label="Phone number" value={ phone } onChange={ phone=> this.setState({ phone }) } />
        <Input label="Password" password value={ password } onChange={ password => this.setState({ password }) } />
        <Button disabled={ disableButton }>Sign up</Button>
        { error ? <Error>{ error }</Error> : null }
        <Link to="/">Back to sign in page</Link>
      </Form>
    )
  }
  renderVerification(){
    const { error } = this.props
    const { code } = this.state
    return (
      <Form onSubmit={ () => this.submitCode() }>
        <div>You should have received an email message with a verification code. Please insert it</div>
        <Input label="Verification Code" value={ code } onChange={ code => this.setState({ code }) } />
        <Button disabled={ !code.length }>Verify</Button>
        { error ? <Error>{ error }</Error> : null }
        <Link to="/">Back to sign in page</Link>
      </Form>
    )  
  }
  render(){
    return this.props.verifyCode ? this.renderVerification() : this.renderForm()
  }
}

export default Register
