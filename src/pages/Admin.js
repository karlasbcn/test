import React from 'react'
import LoggedPage from '../ui/LoggedPage'

const Admin = ({ user, signOut, uploadAvatar }) => (
  <LoggedPage isAdminPage user={ user } signOut={ signOut } uploadAvatar={ uploadAvatar }>
    Admin page
  </LoggedPage>
)

export default Admin
