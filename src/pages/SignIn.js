import React from 'react'
import { Link } from 'react-router-dom'
import Form from '../ui/Form'
import Button from '../ui/Button'
import Input from '../ui/Input'
import Error from '../ui/Error'

class SignIn extends React.Component{
  state = {
    username : '',
    password : ''
  }
  submit(){
    const { username, password } = this.state
    this.props.signIn(username, password)
  }
  render(){
    const { username, password } = this.state
    const { error } = this.props
    return (
      <Form onSubmit={ () => this.submit() }>
        <div>If you already created an account, please insert your username and password</div>
        <Input label="Username" value={ username } onChange={ username => this.setState({ username }) } />
        <Input label="Password" password value={ password } onChange={ password => this.setState({ password }) } />
        <Button disabled={ !(username.length && password.length) }>Sign In</Button>
        { error ? <Error>{ error }</Error> : null }
        <Link to="/register">Create an account</Link>
      </Form>
    )
  }
}

export default SignIn
