import { createGlobalStyle } from 'styled-components'

export const theme = {
  colors : {
    grey : '#565656',
    lightGrey : '#d3d3d3',
    teal : '#008080',
    lightTeal: '#00a0a0',
    red : '#c21609',
    white : '#fefefe',
    yellow : '#ffcc00',
    blue : '#237cbf'
  }
}

export const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Raleway:400,700');
  html{
    height: 100%;
  }
  body{
    margin: 0;
    padding: 0;
    width: 100vw;
    min-height: 100%;
    & *{
      font-family: 'Raleway', sans-serif;
    }
  }
  #root{
    margin: 0;
    font-size: 14px;
    height: 100%;
    width: 100%;
    letter-spacing: normal;
    color: ${ ({ theme }) => theme.colors.grey };
    background-color: ${ ({ theme }) => theme.colors.white };
    display: flex;
    flex-direction: column;
  }
`