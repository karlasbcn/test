import { Auth, Storage } from 'aws-amplify'

const checkImageExists = src => new Promise((resolve, reject) => {
  const img = new Image()
  img.onload = resolve
  img.onerror = reject
  img.src = src
})


export const signIn = async (username, password) => {
  try{
    return await Auth.signIn(username, password)
  }
  catch (error){
    return { error : error.message }
  }
}

export const getCurrentUser = async () => {
  try{
    const { username, attributes, signInUserSession } = await Auth.currentAuthenticatedUser()
    const { phone_number, email } = attributes
    const user = {
      username,
      email,
      phone : phone_number,
      hasAdminRole : signInUserSession.accessToken.payload['cognito:groups'].includes('admins')
    }
    try{
      const avatarUrl = await Storage.get(username)
      await checkImageExists(avatarUrl)
      user.avatarUrl = avatarUrl
    }
    catch{
      user.avatarUrl = false
    }
    finally{
      return user
    }
  }
  catch{
    return false
  }
}

export const signUp = async (username, password, phone, email) => {
  try{
    await Auth.signUp({
      username,
      password,
      attributes : { 
        email,
        phone_number : phone 
      }
    })
    return true
  }
  catch (error){
    return { error : error.message }
  }
}

export const confirmCode = async (username, code) => {
  try{
    await Auth.confirmSignUp(username, code)
    return true
  }
  catch (error){
    return { error : error.message }
  }
} 

export const signOut = async () => {
  try{
    await Auth.signOut()
    return true
  }
  catch{
    return false
  }
}

export const uploadAvatar = async (file, username) => {
  try{
    await Storage.put(username, file, {  contentType: `image/${ file.name.split('.').pop() }` })
    return true
  }
  catch{
    return false
  }
}
