# Prerequisites

You need to install [NodeJS](https://nodejs.org/en/download/), [yarn](https://yarnpkg.com/lang/en/) (but NPM will work as well) and [Amplify](https://aws.amazon.com/es/amplify/) CLI 

# How to run in your local machine

``yarn install`` and ``yarn start``

# Online version

You can find it [here](http://test-ohpen-20190714103106-hostingbucket-dev.s3-website-eu-west-1.amazonaws.com/)

# User roles

Users which create new account do not get the "admin" role. However, there's an admin user (username ``admin``,  password ``12345678``) with this role, which was set through the Cognito user groups.

There are two different pages: "home" (``/home``) and "admin" (``/home/admin``) . Only admin users can access the second one. If a not admin user tries to access admin page, he/she will be redirected to home page.

# Avatar

Avatar images can be selected by clicking the avatar circle.

# What's missing

- Unit testing and storybook: No time, sorry :_(
- SMS user verification: It was done (you can check previous commits), but it turns amazon stopped sending SMS messages after the 10th one, so I switched it to email verification. I don't know if it's some kind of free tier limit, or I'm just doing something wrong...